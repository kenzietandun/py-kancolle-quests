#!/bin/bash

sqlite3 ~/.config/kquest.db 'select quest_id from quest;' > quest_id.tmp

while read -r line
do
  until ./main.py --find "$line"
  do
    echo "Retrying ${line}"
  done
done < quest_id.tmp
