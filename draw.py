#!/usr/bin/env python3

import os
import math
from PIL import Image, ImageDraw, ImageFont
from anytree import Node, RenderTree
from anytree.exporter import DotExporter

def find_requirements(query, c, cache={}):
    """ Finds requirement of a quest recursively """
    if query == "":
        return ""
    elif query in cache:
        return cache[query]
    else:
        c.execute("""SELECT quest_req FROM quest
                     WHERE quest_id = (?)""",
                  (query.upper(),))
        requirements = c.fetchall()
        if not requirements:
            return {}
        requirements = requirements[0][0]
        requirements = requirements.split(',')
        cache[query] = requirements
        for requirement in requirements:
            find_requirements(requirement, c)

    return cache

def draw_requirement_tree(req_dict, parent=None, node_name=None,
        history=[]):
    """ Draws requirement tree recursively """
    if not isinstance(parent, Node):
        parent = Node(parent)

    if not node_name in req_dict:
        return
    elif node_name in history:
        return
    else:
        history.append(node_name)
        children_list = []
        for q in req_dict[node_name]:
            children_list.append((Node(q), q))
        parent.children = [req[0] for req in children_list]
        for q in parent.children:
            for node_req, req in children_list:
                if node_req == q:
                    draw_requirement_tree(
                            req_dict, 
                            parent=q, 
                            node_name=req)

    return parent

def draw_quest_descriptions(quests_dict, c, filename="desc.png", 
        max_height=0):
    """ Create an image of quest descriptions
    which can be combined with quest dependencies later """

    def indent_helper(quest_task, max_length=50):
        strings = quest_task.replace('\n', '')
        strings = strings.split()

        indented_string = ""
        current_pos = 0
        for string in strings:
            if len(string) + current_pos >= max_length:
                indented_string += '\n'
                current_pos = 0
            indented_string += string + ' '
            current_pos += len(string) + 1

        return indented_string

    quests = []
    for quest_id, quest_deps in quests_dict.items():
        quests.append(quest_id)
        quests.extend(quest_deps)

    quests = sorted(list(set(quests)))
    quests = list(filter(None, quests))

    quest_desc = {}
    total_lines = 0
    for quest in quests:
        print(quest)
        c.execute("""SELECT quest_title, quest_task FROM quest
                     WHERE quest_id == (?)""", (quest.upper(),))
        q_title, q_task = c.fetchone()
        q_title_indented = indent_helper(q_title, max_length=50)
        q_task_indented = indent_helper(q_task, max_length=56)
        quest_desc[quest] = q_title_indented, q_task_indented

        total_lines += 3 \
                + q_title_indented.count('\n') \
                + q_task_indented.count('\n')

    # Font size
    font_size = 20
    
    # Spacing between texts
    line_spacing = font_size + 10

    # Calculate total image length
    total_image_length = total_lines * line_spacing

    # Calculate how much width do we need to fit all descriptions
    total_pages = math.ceil(total_image_length / max_height)
    if total_pages <= 1:
        total_pages = 1

    image_page_width = 39 * font_size
    image_width = total_pages * image_page_width

    # Add 200 just in case there are long quest tasks
    image_length = max_height + 200

    img = Image.new("RGB", (image_width, image_length), color="white")
    print("Created canvas {}x{}".format(image_width, image_length))
    font_location = os.path.dirname(os.path.abspath(__file__))
    default_font = ImageFont.truetype(
            font_location + "/SourceHanSansJP-Regular.otf", font_size)
    d = ImageDraw.Draw(img)

    # Color presets
    black = (0, 0, 0)
    red = (231, 76, 60)
    blue = (52, 152, 219)

    # Offsets where the text will be printed
    y_offset = 1
    x_offset = 30
    for quest, desc in quest_desc.items():
        print("Printing {} at {}, {}".format(quest, x_offset, y_offset))
        q_title, q_task = desc

        d.text((x_offset, y_offset), quest, fill=black, font=default_font)
        y_offset += line_spacing

        d.text((x_offset, y_offset), q_title, fill=red, font=default_font)
        y_offset += line_spacing * (1 + q_title.count('\n'))

        d.text((x_offset, y_offset), q_task, fill=blue, font=default_font)
        y_offset += line_spacing * (1 + q_task.count('\n'))

        if y_offset >= max_height:
            y_offset = 1
            x_offset += image_page_width

    img.save(filename)

def export_to_png(data, filename="test.png"):
    """ Exports Tree to png """
    DotExporter(data).to_picture(filename)

def get_image_dimensions(image):
    """ Get image width and height """
    img = Image.open(image)
    return img.size

def combine_images(deps_file, desc_file, out="out.png"):
    """ Combines deps_file and desc_file,
    requires ImageMagick to be installed """
    os.system("convert '{file1}' '{file2}' +append '{file3}'".format(
        file1=deps_file,
        file2=desc_file,
        file3=out))
