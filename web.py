#!/usr/bin/env python3

import os
import draw
import sqlite3
import time
import main
from flask import Flask  
from flask import render_template  
from flask import request  
from flask import send_file  
from flask import redirect  
from flask import abort

HOME = os.environ.get("HOME")
HERE = os.path.dirname(os.path.abspath(__file__))

app = Flask(__name__)

@app.route('/')
def mainpage():
    """ Main page for website """
    return render_template("main.html")

@app.route("/submit", methods=["POST"])
def process_form():
    """ Submit method """
    query = request.form["quest"]
    query = query.replace(' ', '')
    query = query.upper()
    return redirect("/graph/final-{}.png".format(query))

@app.route("/graph/<query>", methods=["GET"])
def get_graph(query):
    """ Checks if graph exists, if not returns 404 """
    if not os.path.isfile("static/{}".format(query)):
        abort(404)
    return send_file("static/{}".format(query),
                     mimetype="image/png",
                     cache_timeout=1)

@app.errorhandler(404)
def error_not_found(e):
    """ 404 error handler """
    return render_template("404.html"), 404

if __name__ == "__main__":
    """ Run flask app """
    app.run(host='0.0.0.0')
