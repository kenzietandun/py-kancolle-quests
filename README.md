## py-kancolle-quests

is a program that finds Kancolle quests dependencies and print them into a pretty graph

<img src="https://gitlab.com/kenzietandun/py-kancolle-quests/raw/master/quest_f46.png">

An example showing quest path for Kikka Jet quest, F46.

## Dependencies

- anytree

- beautifulsoup4

- requests

- ImageMagick (Install with your package manager)

- graphviz (Install with your package manager)

## Install

- Make sure you have python3, pip3, git and virtualenv installed

- Clone the repo with `git clone https://gitlab.com/kenzietandun/py-kancolle-quests`

- `cd py-kancolle-quests`

- Create a new virtualenv with `virtualenv -p python3 ./`

- Activate the virtualenv, `source bin/activate`

- Install dependencies with `pip install -r req.txt`

## Usage

Run `./main.py --find {QUEST}`. For example, to find dependencies for quest F46, run `./main.py --find F46`. If there aren't any errors, a new file `test.png` showing the dependencies will be created in the same directory.

Run `./main.py --sync` to synchronise any updates from the Kancolle wikia website, although in the current time of writing the script has problem recognising quests from section F. 

Please report if you were to find any bugs/errors to the issue tracker.

## Future updates

- [ ] Quest description
