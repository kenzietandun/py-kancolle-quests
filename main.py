#!/usr/bin/env python3

import os
import draw
import quest
import sqlite3
import argparse

def init_db():
    """ Initialise db """
    home_dir = os.getenv("HOME")
    db_file = home_dir + "/.config/kquest.db"

    if not os.path.isfile(db_file):
        os.mknod(db_file)

    conn = sqlite3.connect(db_file)
    c = conn.cursor()

    c.execute("""CREATE TABLE IF NOT EXISTS quest
                 (quest_id TEXT PRIMARY KEY,
                  quest_title TEXT,
                  quest_task TEXT,
                  quest_resources TEXT,
                  quest_rewards TEXT,
                  quest_req TEXT,
                  quest_unlock TEXT)""")
    conn.commit()

    return conn, c

def write_to_db(data, conn, c):
    """ Writes data into sqlite database """
    q_id = data.quest_id
    q_title = data.quest_title
    q_task = data.quest_task
    q_resource = data.resource
    q_reward = data.reward
    q_req, q_unlock = data.req
    q_req = ','.join(q_req)
    q_unlock = ','.join(q_unlock)

    c.execute("""INSERT OR IGNORE INTO quest
                 VALUES 
                 ( (?), (?), (?), (?), (?), (?), (?) )""", 
                 (q_id, q_title, q_task, q_resource, 
                  q_reward, q_req, q_unlock))

def process_requirements(query, c):
    """ Helper function to generate quest requirement png 
    queries are cleaned by removing spaces and set to uppercase

    the function does not do anything if the required png is 
    already generated before """
    query = query.replace(' ', '')
    query = query.upper()

    outfile = "static/final-{}.png".format(query)
    if os.path.isfile(outfile):
        print("File already exists at {}".format(outfile))
        return

    req_dict = draw.find_requirements(query, c)
    final_tree = draw.draw_requirement_tree(
            req_dict, 
            parent=query, 
            node_name=query)
    deps_filename = "deps-{}.png".format(query)
    draw.export_to_png(final_tree, 
            filename=deps_filename)
    deps_dimensions = draw.get_image_dimensions(deps_filename)
    _, deps_height = deps_dimensions

    desc_filename = "desc-{}.png".format(query)
    draw.draw_quest_descriptions(
            req_dict, c, 
            filename=desc_filename,
            max_height=deps_height)
    draw.combine_images(deps_filename, desc_filename,
            out=outfile)
    # Remove temp files
    os.remove(deps_filename)
    os.remove(desc_filename)

def parse_arguments():
    """ Parses argument passed to main.py """
    parser = argparse.ArgumentParser()
    parser.add_argument("--sync", help="Sync database with Kancolle Wikia",
            action="store_true")
    parser.add_argument("-f", "--find", help="Get quest chain data")
    args = parser.parse_args()
    conn, c = init_db()
    if args.sync:
        quests_list = quest.download_quests()
        for quest_data in quests_list:
            quest_obj = quest.Quest(quest_data)
            quest_obj.parse_quest_data()
            write_to_db(quest_obj, conn, c)

        conn.commit()
            
    elif args.find:
        query = args.find
        process_requirements(query, c)

    args = parser.parse_args()

def main():
    """ Main method """
    parse_arguments()

if __name__ == "__main__":
    main()
