#!/usr/bin/env python3

import re
from bs4 import BeautifulSoup as bs

class Quest:
    def __init__(self, quest_soup):
        """ Quest initialiser, takes a 
        quest html as an parameter """
        self.quest_soup = quest_soup

    def parse_quest_data(self):
        """ Parses quest data 
        Parsed data can be accessed through
        self.<<fields>> """
        soup = bs(self.quest_soup, "html.parser")

        # Parse Quest ID
        quest_id = soup.find("td", {"rowspan": "2"}).text
        self.quest_id = quest_id.strip().upper()

        # Parse Quest Title
        quest_title = soup.find("td", {"colspan": "11"}).text
        self.quest_title = quest_title.strip()
        
        details_row = soup.findAll("tr")[1]
        
        quest_details = []
        for index, column in enumerate(details_row.findAll("td")):
            if index % 2 == 0:
                if index == 4:
                    reward = self.parse_rewards(column)
                    quest_details.append(reward)
                elif index == 6:
                    req = self.parse_req(column)
                    quest_details.append(req)
                else:
                    quest_details.append(column.text.strip())

        self.quest_task, self.resource, self.reward, self.req = quest_details

    def parse_req(self, requirement_soup):
        """ Try to parse quest requirement and unlocks """

        def split_string_to_list(string):
            """ Splits a string into list and remove whitespaces """
            if not ',' in string:
                return [string]
            
            clean_string = string.replace(' ', '')
            clean_string = clean_string.split(',')
            return clean_string

        total_unlocks, total_requires = "", ""

        unlock_pattern = re.compile("Unlocks: (([A-G][dwmqDWMQ]?\d+\,?\s?)+)")
        total_unlocks = unlock_pattern.findall(requirement_soup.text)
        if total_unlocks:
            total_unlocks = [quest_id.strip() for quest_id in total_unlocks[0]]
            total_unlocks = total_unlocks[:-1][0]
            total_unlocks = split_string_to_list(total_unlocks)

        require_pattern = re.compile("Requires: (([A-G][dwmqDWMQ]?\d+\,?\s?)+)")
        total_requires = require_pattern.findall(requirement_soup.text)
        if total_requires:
            total_requires = [quest_id.strip() for quest_id in total_requires[0]]
            total_requires = total_requires[:-1][0]
            total_requires = split_string_to_list(total_requires)

        return total_requires, total_unlocks

    def parse_rewards(self, reward_soup):
        """ Try to parse rewards, might be inaccurate due to how the 
        wiki was coded """
        total_items_pattern = re.compile("[×xX]\d+")
        total_items = total_items_pattern.findall(reward_soup.text)
        
        if not total_items:
            return reward_soup.text.strip()
        
        rewards = []
        for img in reward_soup.findAll("img"):
            reward_name = img.get("alt")
            if reward_name and reward_name not in rewards:
                rewards.append(reward_name)

        final_rewards = []
        if rewards:
            for index, reward in enumerate(rewards): 
                if len(total_items) > index:
                    final_rewards.append(reward + ' ' + total_items[index])
                else:
                    final_rewards.append(reward)

        return ', '.join(final_rewards)

    def __str__(self):
        return ("Quest ID: {q_id}\n"
               "Title: {title}\n"
               "Task: {task}\n"
               "Resources: {resource}\n"
               "Rewards: {reward}\n"
               "Requirement/Unlocks: {req}".format(
                       q_id = self.quest_id,
                       title = self.quest_title,
                       task = self.quest_task,
                       resource = self.resource,
                       reward = self.reward,
                       req = self.req))

    def __repr__(self):
        return self.quest_id

def download_quests():
    """ Download quests list from kancolle wikia """
    import requests


    quest_data_list = []
    main_quests_url = "http://kancolle.wikia.com/wiki/Quests"
    page = requests.get(main_quests_url)
    page_text = page.text
    page_soup = bs(page_text, "lxml")

    tables = page_soup.findAll("table", {"cellspacing": "0"})
    quest_data = ""
    for table in tables:
        for index, tr in enumerate(table.findAll("tr", limit=None)):
            if index == 0:
                continue
            quest_data += str(tr)
            if index % 2 == 0:
                quest_data_list.append(quest_data)
                quest_data = ""
        break

    return quest_data_list

if __name__ == "__main__":
    download_quests()

### TEST CASES
#import test
#
#quest = Quest(test.test7)
#quest.parse_quest_data()
#print(quest)
##print()
#quest = Quest(test.test2)
#quest.parse_quest_data()
##print(quest)
##print()
#quest = Quest(test.test3)
#quest.parse_quest_data()
#print(quest)
##print()
#quest = Quest(test.test4)
#quest.parse_quest_data()
##print(quest)
##print()
#quest = Quest(test.test5)
#quest.parse_quest_data()
##print(quest)
##print()
#quest = Quest(test.test6)
#quest.parse_quest_data()
##print(quest)
