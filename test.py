#!/usr/bin/env python3

test1 = """<tr id="A1" class="quest_A">
<td rowspan="2"><b>A1</b>
</td><td colspan="11"><b><span lang="ja">はじめての「編成」！</span></b><br><i>The First Organization</i>
</td></tr>
<tr class="quest_details_A">
<td>Have 2 ships in your main fleet.
</td><td>
</td><td>20 / 20 / 0 / 0
</td><td>
</td><td><i><span class="advanced-tooltip tooltips-init-complete"><a href="/wiki/Shirayuki">Shirayuki</a></span></i>
</td><td>
</td><td>Unlocks: <a href="#A2">A2</a>
</td></tr>"""

test2 = """<tr id="A23" class="quest_A">
<td rowspan="2"><b>A23</b>
</td><td colspan="11"><b><span lang="ja">「第五航空戦隊」を編成せよ！</span></b><br><i>Organize the 5th Carrier Division</i>
</td></tr>
<tr class="quest_details_A">
<td>Have both <span class="advanced-tooltip tooltips-init-complete"><a href="/wiki/Shoukaku">Shoukaku</a></span>, <span class="advanced-tooltip tooltips-init-complete"><a href="/wiki/Zuikaku">Zuikaku</a></span>, and 2 <a href="/wiki/Ship_Class#Destroyer_.28DD.29" title="Ship Class">Destroyers</a> in your main fleet.
</td><td>
</td><td>300 / 0 / 0 / 300
</td><td>
</td><td><i><span class="advanced-tooltip tooltips-init-complete"><a href="/wiki/Emergency_Repair_Personnel" class="image image-thumbnail link-internal"><img src="https://vignette.wikia.nocookie.net/kancolle/images/2/21/Emergency_Repair_Personnel_042_Card.png/revision/latest/scale-to-width-down/30?cb=20140226051500" alt="Emergency Repair Personnel 042 Card" class="lzyPlcHld lzyTrns lzyLoaded" data-image-key="Emergency_Repair_Personnel_042_Card.png" data-image-name="Emergency Repair Personnel 042 Card.png" data-src="https://vignette.wikia.nocookie.net/kancolle/images/2/21/Emergency_Repair_Personnel_042_Card.png/revision/latest/scale-to-width-down/30?cb=20140226051500" onload="if(typeof ImgLzy==='object'){ImgLzy.load(this)}" width="30" height="30"><noscript><img src="https://vignette.wikia.nocookie.net/kancolle/images/2/21/Emergency_Repair_Personnel_042_Card.png/revision/latest/scale-to-width-down/30?cb=20140226051500" 	 alt="Emergency Repair Personnel 042 Card"  	class="" 	 	data-image-key="Emergency_Repair_Personnel_042_Card.png" 	data-image-name="Emergency Repair Personnel 042 Card.png" 	 	 width="30"  	 height="30"  	 	 	 	></noscript></a><a href="/wiki/Emergency_Repair_Personnel">Emergency Repair Personnel</a></span></i>
</td><td>
</td><td>Requires: <a href="#A22">A22</a><br>Unlocks: <a href="#A41">A41</a>, <a href="#A46">A46</a>, <a href="#A56">A56</a>, <a href="#B15">B15</a>
</td></tr>"""

test3 = """<tr id="B83" class="quest_B">
<td rowspan="2"><b>B83</b>
</td><td colspan="11"><b><span lang="ja">南西諸島防衛線を増強せよ！</span></b><br><i>Reinforce the Nansei Islands Defense Line!</i>
</td></tr>
<tr class="quest_details_B">
<td>Sortie a fleet with a <a href="/wiki/Ship_Class#Seaplane_Tender_.28AV.29" title="Ship Class">Seaplane Tender</a> or  <a href="/wiki/Ship_Class#Aviation_Cruiser_.28CAV.29" title="Ship Class">Aviation Cruiser</a> as flagship, up to 5 additional ships to <a href="/wiki/1-4" title="1-4" class="mw-redirect">World 1-4</a> and obtain an S-rank victory at the boss node
</td><td>
</td><td>300 / 300 / 0 / 0
</td><td>
</td><td><i><a href="/wiki/Furniture" class="image image-thumbnail link-internal" title="Furniture"><img src="https://vignette.wikia.nocookie.net/kancolle/images/8/81/Furniture_box_medium.png/revision/latest/scale-to-width-down/30?cb=20140127223821" alt="Furniture box medium" class="lzyPlcHld lzyTrns lzyLoaded" data-image-key="Furniture_box_medium.png" data-image-name="Furniture box medium.png" data-src="https://vignette.wikia.nocookie.net/kancolle/images/8/81/Furniture_box_medium.png/revision/latest/scale-to-width-down/30?cb=20140127223821" onload="if(typeof ImgLzy==='object'){ImgLzy.load(this)}" width="30" height="30"><noscript><img src="https://vignette.wikia.nocookie.net/kancolle/images/8/81/Furniture_box_medium.png/revision/latest/scale-to-width-down/30?cb=20140127223821" 	 alt="Furniture box medium"  	class="" 	 	data-image-key="Furniture_box_medium.png" 	data-image-name="Furniture box medium.png" 	 	 width="30"  	 height="30"  	 	 	 	></noscript></a>×1<br><a href="/wiki/Furniture" class="image image-thumbnail link-internal" title="Furniture"><img src="https://vignette.wikia.nocookie.net/kancolle/images/f/fa/Furniture_fairy.png/revision/latest/scale-to-width-down/30?cb=20141108221801" alt="Furniture fairy" class="lzyPlcHld lzyTrns lzyLoaded" data-image-key="Furniture_fairy.png" data-image-name="Furniture fairy.png" data-src="https://vignette.wikia.nocookie.net/kancolle/images/f/fa/Furniture_fairy.png/revision/latest/scale-to-width-down/30?cb=20141108221801" onload="if(typeof ImgLzy==='object'){ImgLzy.load(this)}" width="30" height="30"><noscript><img src="https://vignette.wikia.nocookie.net/kancolle/images/f/fa/Furniture_fairy.png/revision/latest/scale-to-width-down/30?cb=20141108221801" 	 alt="Furniture fairy"  	class="" 	 	data-image-key="Furniture_fairy.png" 	data-image-name="Furniture fairy.png" 	 	 width="30"  	 height="30"  	 	 	 	></noscript></a><a href="/wiki/Furniture" title="Furniture">Furniture Fairy</a>×1</i>
</td><td>
</td><td>Requires:&nbsp;?
</td></tr>"""

test4 = """<tr id="C1" class="quest_C">
<td rowspan="2"><b>C1</b>
</td><td colspan="11"><b><span lang="ja">はじめての「演習」！</span></b><br><i>The First Exercise</i>
</td></tr>
<tr class="quest_details_C">
<td>Challenge another fleet in Exercise
</td><td>
</td><td>10 / 10 / 0 / 0
</td><td>
</td><td><i><a href="/wiki/Construction" class="image image-thumbnail link-internal" title="Construction"><img src="https://vignette.wikia.nocookie.net/kancolle/images/0/09/Development_material.png/revision/latest/scale-to-width-down/30?cb=20141026163009" alt="Development material" class="lzyPlcHld lzyTrns lzyLoaded" data-image-key="Development_material.png" data-image-name="Development material.png" data-src="https://vignette.wikia.nocookie.net/kancolle/images/0/09/Development_material.png/revision/latest/scale-to-width-down/30?cb=20141026163009" onload="if(typeof ImgLzy==='object'){ImgLzy.load(this)}" width="30" height="30"><noscript><img src="https://vignette.wikia.nocookie.net/kancolle/images/0/09/Development_material.png/revision/latest/scale-to-width-down/30?cb=20141026163009" 	 alt="Development material"  	class="" 	 	data-image-key="Development_material.png" 	data-image-name="Development material.png" 	 	 width="30"  	 height="30"  	 	 	 	></noscript></a>×1</i>
</td><td>
</td><td>
</td></tr>"""

test5 = """<tr id="C4" class="quest_C">
<td rowspan="2"><b>C4</b>
</td><td colspan="11"><b><span lang="ja">大規模演習</span></b><br><i>Large-scale Exercises</i>
</td></tr>
<tr class="quest_details_C">
<td>Get 20 victories in Exercises within the same week
</td><td>
</td><td>200 / 200 / 200 / 200
</td><td>
</td><td><i><a href="/wiki/Construction" class="image image-thumbnail link-internal" title="Construction"><img src="https://vignette.wikia.nocookie.net/kancolle/images/0/09/Development_material.png/revision/latest/scale-to-width-down/30?cb=20141026163009" alt="Development material" class="lzyPlcHld lzyTrns lzyLoaded" data-image-key="Development_material.png" data-image-name="Development material.png" data-src="https://vignette.wikia.nocookie.net/kancolle/images/0/09/Development_material.png/revision/latest/scale-to-width-down/30?cb=20141026163009" onload="if(typeof ImgLzy==='object'){ImgLzy.load(this)}" width="30" height="30"><noscript><img src="https://vignette.wikia.nocookie.net/kancolle/images/0/09/Development_material.png/revision/latest/scale-to-width-down/30?cb=20141026163009" 	 alt="Development material"  	class="" 	 	data-image-key="Development_material.png" 	data-image-name="Development material.png" 	 	 width="30"  	 height="30"  	 	 	 	></noscript></a>×2 <a href="/wiki/Akashi%27s_Improvement_Arsenal" class="image image-thumbnail link-internal" title="Akashi's Improvement Arsenal"><img src="https://vignette.wikia.nocookie.net/kancolle/images/b/bb/Improvement_Materials.png/revision/latest/scale-to-width-down/30?cb=20141025160403" alt="Improvement Materials" class="lzyPlcHld lzyTrns lzyLoaded" data-image-key="Improvement_Materials.png" data-image-name="Improvement Materials.png" data-src="https://vignette.wikia.nocookie.net/kancolle/images/b/bb/Improvement_Materials.png/revision/latest/scale-to-width-down/30?cb=20141025160403" onload="if(typeof ImgLzy==='object'){ImgLzy.load(this)}" width="30" height="30"><noscript><img src="https://vignette.wikia.nocookie.net/kancolle/images/b/bb/Improvement_Materials.png/revision/latest/scale-to-width-down/30?cb=20141025160403" 	 alt="Improvement Materials"  	class="" 	 	data-image-key="Improvement_Materials.png" 	data-image-name="Improvement Materials.png" 	 	 width="30"  	 height="30"  	 	 	 	></noscript></a>×1</i>
</td><td>
</td><td>Requires: <a href="#C2">C2</a><br>Unlocks: <a href="#F42">F42</a> <br><b>Weekly Quest</b>
</td></tr>"""

test6 = """<tr id="F41" class="quest_F">
<td rowspan="2"><b>F41</b>
</td><td colspan="11"><b><span lang="ja">「洋上補給」物資の調達</span></b><br><i>Monthly Procurement of Maritime Supply Goods</i>
</td></tr>
<tr class="quest_details_F">
<td>Prepare 750 fuel, 750 ammo, two <span class="advanced-tooltip tooltips-init-complete"><a href="/wiki/Drum_Canister_(Transport_Use)">Drum Canister (Transport Use)</a></span>, one <span class="advanced-tooltip tooltips-init-complete"><a href="/wiki/Type_91_Armor_Piercing_Shell">Type 91 Armor Piercing Shell</a></span> in your inventory, scrap one <span class="advanced-tooltip tooltips-init-complete"><a href="/wiki/Type_3_Shell">Type 3 Shell</a></span>.<br>
<p><b>※Equipment must be unlocked.</b><br>
<b>※Consumes all the resources and equipment.</b><br>
<b>※It doesn't matter whether you scrap the shell before or after procuring the other items.</b>
</p>
</td><td>
</td><td>0 / 0 / 0 / 0
</td><td>
</td><td><i><a href="/wiki/Underway_Replenishment" class="image image-thumbnail link-internal" title="Underway Replenishment"><img src="https://vignette.wikia.nocookie.net/kancolle/images/5/5d/Supplies_Icon.png/revision/latest/scale-to-width-down/30?cb=20150810143433" alt="Supplies Icon" class="lzyPlcHld lzyTrns lzyLoaded" data-image-key="Supplies_Icon.png" data-image-name="Supplies Icon.png" data-src="https://vignette.wikia.nocookie.net/kancolle/images/5/5d/Supplies_Icon.png/revision/latest/scale-to-width-down/30?cb=20150810143433" onload="if(typeof ImgLzy==='object'){ImgLzy.load(this)}" width="30" height="30"><noscript><img src="https://vignette.wikia.nocookie.net/kancolle/images/5/5d/Supplies_Icon.png/revision/latest/scale-to-width-down/30?cb=20150810143433" 	 alt="Supplies Icon"  	class="" 	 	data-image-key="Supplies_Icon.png" 	data-image-name="Supplies Icon.png" 	 	 width="30"  	 height="30"  	 	 	 	></noscript></a><a href="/wiki/Underway_Replenishment" title="Underway Replenishment">Underway Replenishment</a>×1</i>
</td><td>
</td><td><b>Monthly Quest</b><br>Requires: <a href="#B54">B54</a>, <a href="#Bw5">Bw5</a>
</td></tr>"""

test7 = """<tr id="F46" class="quest_F">
<td rowspan="2"><b>F46</b>
</td><td colspan="11"><b><span lang="ja">噴式戦闘爆撃機の開発</span></b><br /><i>Development of Jet-type Bomber</i>
</td></tr>
<tr class="quest_details_F">
<td>Prepare 2 <a href="/wiki/New_Technology_Aircraft_Blueprint" title="New Technology Aircraft Blueprint">New Technology Aircraft Blueprint</a> and 1 <span class="advanced-tooltip"><a href="/wiki/Ne_Type_Engine" title="Ne Type Engine">Ne Type Engine</a><span class="tooltip-contents"><a href="/wiki/Ne_Type_Engine" 	class="image image-thumbnail link-internal" 	 title="Ne Type Engine"  	 	><img src="data:image/gif;base64,R0lGODlhAQABAIABAAAAAP///yH5BAEAAAEALAAAAAABAAEAQAICTAEAOw%3D%3D" 	 alt="Ne Type Engine Card"  	class="lzy lzyPlcHld " 	 	data-image-key="Ne_Type_Engine_Card.png" 	data-image-name="Ne Type Engine Card.png" 	 data-src="https://vignette.wikia.nocookie.net/kancolle/images/3/36/Ne_Type_Engine_Card.png/revision/latest?cb=20151222110949"  	 width="180"  	 height="180"  	 	 	 onload="if(typeof ImgLzy===&#39;object&#39;){ImgLzy.load(this)}"  	><noscript><img src="https://vignette.wikia.nocookie.net/kancolle/images/3/36/Ne_Type_Engine_Card.png/revision/latest?cb=20151222110949" 	 alt="Ne Type Engine Card"  	class="" 	 	data-image-key="Ne_Type_Engine_Card.png" 	data-image-name="Ne Type Engine Card.png" 	 	 width="180"  	 height="180"  	 	 	 	></noscript></a></span></span> in your inventory, scrap 3 <span class="advanced-tooltip"><a href="/wiki/Shiden_Kai_2" title="Shiden Kai 2">Shiden Kai 2</a><span class="tooltip-contents"><a href="/wiki/Shiden_Kai_2" 	class="image image-thumbnail link-internal" 	 title="Shiden Kai 2"  	 	><img src="data:image/gif;base64,R0lGODlhAQABAIABAAAAAP///yH5BAEAAAEALAAAAAABAAEAQAICTAEAOw%3D%3D" 	 alt="Shiden Kai 2 055 Card"  	class="lzy lzyPlcHld " 	 	data-image-key="Shiden_Kai_2_055_Card.png" 	data-image-name="Shiden Kai 2 055 Card.png" 	 data-src="https://vignette.wikia.nocookie.net/kancolle/images/7/74/Shiden_Kai_2_055_Card.png/revision/latest?cb=20150813191618"  	 width="260"  	 height="260"  	 	 	 onload="if(typeof ImgLzy===&#39;object&#39;){ImgLzy.load(this)}"  	><noscript><img src="https://vignette.wikia.nocookie.net/kancolle/images/7/74/Shiden_Kai_2_055_Card.png/revision/latest?cb=20150813191618" 	 alt="Shiden Kai 2 055 Card"  	class="" 	 	data-image-key="Shiden_Kai_2_055_Card.png" 	data-image-name="Shiden Kai 2 055 Card.png" 	 	 width="260"  	 height="260"  	 	 	 	></noscript></a></span></span>. <br />
<p><b>※The New Technology Aircraft Blueprints &amp; Ne Type Engine are consumed upon quest completion</b><br />
</p>
</td><td>
</td><td>0 / 0 / 0 / 100
</td><td>
</td><td><i><span class="advanced-tooltip"><a href="/wiki/Kikka_Kai" 	class="image image-thumbnail link-internal" 	 title="Kikka Kai"  	 	><img src="data:image/gif;base64,R0lGODlhAQABAIABAAAAAP///yH5BAEAAAEALAAAAAABAAEAQAICTAEAOw%3D%3D" 	 alt="Kikka Kai 200 Card"  	class="lzy lzyPlcHld " 	 	data-image-key="Kikka_Kai_200_Card.png" 	data-image-name="Kikka Kai 200 Card.png" 	 data-src="https://vignette.wikia.nocookie.net/kancolle/images/b/b0/Kikka_Kai_200_Card.png/revision/latest/scale-to-width-down/30?cb=20161209123304"  	 width="30"  	 height="30"  	 	 	 onload="if(typeof ImgLzy===&#39;object&#39;){ImgLzy.load(this)}"  	><noscript><img src="https://vignette.wikia.nocookie.net/kancolle/images/b/b0/Kikka_Kai_200_Card.png/revision/latest/scale-to-width-down/30?cb=20161209123304" 	 alt="Kikka Kai 200 Card"  	class="" 	 	data-image-key="Kikka_Kai_200_Card.png" 	data-image-name="Kikka Kai 200 Card.png" 	 	 width="30"  	 height="30"  	 	 	 	></noscript></a><a href="/wiki/Kikka_Kai" title="Kikka Kai">Kikka Kai</a><span class="tooltip-contents"><a href="/wiki/Kikka_Kai" 	class="image image-thumbnail link-internal" 	 title="Kikka Kai"  	 	><img src="data:image/gif;base64,R0lGODlhAQABAIABAAAAAP///yH5BAEAAAEALAAAAAABAAEAQAICTAEAOw%3D%3D" 	 alt="Kikka Kai 200 Card"  	class="lzy lzyPlcHld " 	 	data-image-key="Kikka_Kai_200_Card.png" 	data-image-name="Kikka Kai 200 Card.png" 	 data-src="https://vignette.wikia.nocookie.net/kancolle/images/b/b0/Kikka_Kai_200_Card.png/revision/latest?cb=20161209123304"  	 width="260"  	 height="260"  	 	 	 onload="if(typeof ImgLzy===&#39;object&#39;){ImgLzy.load(this)}"  	><noscript><img src="https://vignette.wikia.nocookie.net/kancolle/images/b/b0/Kikka_Kai_200_Card.png/revision/latest?cb=20161209123304" 	 alt="Kikka Kai 200 Card"  	class="" 	 	data-image-key="Kikka_Kai_200_Card.png" 	data-image-name="Kikka Kai 200 Card.png" 	 	 width="260"  	 height="260"  	 	 	 	></noscript></a></span></span> x1,<br /> <a href="/wiki/Construction" 	class="image image-thumbnail link-internal" 	 title="Construction"  	 	><img src="data:image/gif;base64,R0lGODlhAQABAIABAAAAAP///yH5BAEAAAEALAAAAAABAAEAQAICTAEAOw%3D%3D" 	 alt="Development material"  	class="lzy lzyPlcHld " 	 	data-image-key="Development_material.png" 	data-image-name="Development material.png" 	 data-src="https://vignette.wikia.nocookie.net/kancolle/images/0/09/Development_material.png/revision/latest/scale-to-width-down/30?cb=20141026163009"  	 width="30"  	 height="30"  	 	 	 onload="if(typeof ImgLzy===&#39;object&#39;){ImgLzy.load(this)}"  	><noscript><img src="https://vignette.wikia.nocookie.net/kancolle/images/0/09/Development_material.png/revision/latest/scale-to-width-down/30?cb=20141026163009" 	 alt="Development material"  	class="" 	 	data-image-key="Development_material.png" 	data-image-name="Development material.png" 	 	 width="30"  	 height="30"  	 	 	 	></noscript></a>×2</i>
</td><td>
</td><td>Requires: <a href="#C3">C3</a>, <a href="#F45">F45</a>, <a href="#B47">B47</a> (??)
</td></tr>"""
